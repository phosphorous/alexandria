<div align="center">

# Alexandria
[![License](https://img.shields.io/github/license/aecsocket/alexandria)](LICENSE)
[![CI](https://img.shields.io/github/actions/workflow/status/aecsocket/alexandria/build.yml)](https://github.com/aecsocket/alexandria/actions/workflows/build.yml)

Multiplatform utilities for Minecraft projects

### [GitHub](https://github.com/aecsocket/alexandria) · [Docs](https://aecsocket.github.io/alexandria)

</div>

Generic utilities for use in my other projects. Not much documentation yet.
