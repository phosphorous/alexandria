plugins {
    id("parent-conventions")
}

group = "io.github.aecsocket"
version = "0.1.1-SNAPSHOT"
description = "Multiplatform utilities for Minecraft projects"
